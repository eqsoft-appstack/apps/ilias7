#!/usr/bin/env bash

# executed in config container with mounted volumes pwd and ilias data volume

if [ ! -f "/tmp/data/config.json" ] ; then
    echo "create ilias config.json"
    envsubst '\
    ${APP}\
    ${APP_NAME}\
    ${DOMAIN}\
' < tpl.config.json > /tmp/data/config.json
    chmod 750 -R /tmp/data
    chown -R www-data:root /tmp/data
else
    echo "ilias config.json already exists"
fi

