server {
    listen ${NGINX_HTTP_PORT};
    server_name ${NGINX_EXTERNAL_SERVERNAME};
    client_max_body_size 256m;
    root ${NGINX_WEB_ROOT};
    index index.php index.htm index.html;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ /\.ht {
        deny all;
    }

    location ~ [^/]\.php(/|$) {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        if (!-f $document_root$fastcgi_script_name) {
            return 404;
        }
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_index index.php;
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        # include the fastcgi_param setting
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # xapiproxy ltiservices add pathinfo
    location ~ ^(.+)(xapiproxy|ltiservices)\.php(.*)$ {
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        add_header 'Access-Control-Expose-Headers' 'ETag';
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # rest
    location ~ ^(/Customizing/global/plugins/Services/UIComponent/UserInterfaceHook/REST/api\.php)(/|$) {
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_split_path_info  ^(/Customizing/global/plugins/Services/UIComponent/UserInterfaceHook/REST/api\.php)(/.+)$;
        try_files  $uri  $fastcgi_script_name  =404;
        # See: http://trac.nginx.org/nginx/ticket/321
        set $path_info $fastcgi_path_info;
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $path_info;
    }

    location ~ \.(js|css|png|jpg|gif|ico|pdf|zip|svg)$ {
        try_files $uri =404;
    }
}
server {
    listen ${NGINX_HTTPS_PORT} ssl default_server;
    server_name ${NGINX_SERVERNAME};
    client_max_body_size 256m;
    root ${NGINX_WEB_ROOT};
    index index.php index.htm index.html;

    ssl_certificate ${NGINX_SSL_CERTIFICATE};
    ssl_certificate_key ${NGINX_SSL_KEY};
    
    location / {
        try_files $uri $uri/ =404;
    }

    location ~ /\.ht {
        deny all;
    }

    location ~ [^/]\.php(/|$) {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        if (!-f $document_root$fastcgi_script_name) {
            return 404;
        }
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_index index.php;
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        # include the fastcgi_param setting
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # xapiproxy ltiservices add pathinfo
    location ~ ^(.+)(xapiproxy|ltiservices)\.php(.*)$ {
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        add_header 'Access-Control-Expose-Headers' 'ETag';
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # rest
    location ~ ^(/Customizing/global/plugins/Services/UIComponent/UserInterfaceHook/REST/api\.php)(/|$) {
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_split_path_info  ^(/Customizing/global/plugins/Services/UIComponent/UserInterfaceHook/REST/api\.php)(/.+)$;
        try_files  $uri  $fastcgi_script_name  =404;
        # See: http://trac.nginx.org/nginx/ticket/321
        set $path_info $fastcgi_path_info;
        fastcgi_pass ${NGINX_FASTCGI_HOST}:${NGINX_FASTCGI_PORT};
        fastcgi_pass_header Authorization;
        fastcgi_read_timeout 905;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $path_info;
    }

    location ~ \.(js|css|png|jpg|gif|ico|pdf|zip|svg)$ {
        try_files $uri =404;
    }
}

